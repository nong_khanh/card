var getMyNameBox = document.querySelector('.input-name__my-name');
var getPartnerNameBox = document.querySelector('.input-name__partner-name');
var getInputMyName = document.querySelector('.input-name--input__my-name');
var getInputPartnerName = document.querySelector('.input-name--input__partner-name');
var getDoneBtn = document.querySelector('.btn-done');
var getStartBtn = document.querySelector('.btn-start');
var getAlert = document.querySelector('.alert');
var getBackBtn = document.querySelector('.back');
var regexSpace = /\s/;

function checkName(name) {
    if(regexSpace.test(name) || name == '') {
        return getAlert.innerHTML = 'Tên không được để trống hoặc chứa khoảng trắng!';
    } else {
        return getAlert.innerHTML = '';
    }
}

getDoneBtn.onclick = function() {
    checkName(getInputMyName.value);
    if(checkName(getInputMyName.value) === '') {
        localStorage.setItem('myName', getInputMyName.value);
        Object.assign(getMyNameBox.style, {
           display: "none",
           animation: "blur 0.5s linear"
        });
        Object.assign(getPartnerNameBox.style, {
            display: "block",
            animation: "see 0.5s linear"
        });
        getBackBtn.style.display = 'inline-block';
    }
    return false;
}


getStartBtn.onclick = function() {
    checkName(getInputPartnerName.value);
    if(checkName(getInputPartnerName.value) === '') {
        localStorage.setItem('partnerName', getInputPartnerName.value);
        window.location.pathname = '/cards_project/content.html';
    }
    return false;
}

getBackBtn.onclick = function() {
    Object.assign(getMyNameBox.style, {
        display: "block",
        animation: "see 0.5s linear"
    });
    Object.assign(getPartnerNameBox.style, {
        display: "none",
        animation: "blur 0.5s linear"
    });
    if(getMyNameBox.style.display === 'block') {
        getBackBtn.style.display = 'none';
    }
    getAlert.innerHTML = '';
}
