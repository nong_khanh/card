var getNames = document.querySelector('.names');
var getSelectItems = document.querySelectorAll('.select-items');
var getSelectItemLove = document.querySelector('.select--love');
var getSelectItemFriend = document.querySelector('.select--friend');
var getCardImgLove = document.querySelector('.card--img__love');
var getCardImgFriend = document.querySelector('.card--img__friend');
var getBackBtn = document.querySelector('.back');
var getCardImgContent = document.querySelector('.card--img__content');
var getNextBtn = document.querySelector('.btn-next');
getNames.innerHTML = `${localStorage.getItem('myName')} và ${localStorage.getItem('partnerName')}`; // in ra ten da duoc nhap tu localStorage
var loveQuestions = [
    'Nếu một ngày cả hai đã cảm thấy mệt mỏi thì bạn và đối phương sẽ duy trì mối quan hệ bằng cách nào?',
    'Bạn cảm thấy không hài lòng nhất ở điểm gì của đối phương?',
    'Tin nhắn/câu nói tình cảm nhất bạn từng nói với người cũ?',
    'Kỉ niệm nào của hai người mà bạn không thể quên?',
    'Đã bao giờ bạn cảm thấy nên chấm dứt mối quan hệ chưa?',
    'Trước khi bắt đầu mối quan hệ bạn đã có bao nhiêu người cũ?',
    'Một điều bạn muốn đối phương thay đổi?',
    'Điều gì khiến bạn cảm thấy hồi hận vì đã nói/làm với đối phương?',
    'Một địa điểm mà hai bạn muốn đi cùng nhau?',
    'Lần cãi nhau to nhất của hai bạn là vì lí do gì?'
];
var friendQuestions = [
    'Khi mới quen nhau cậu có nghĩ mình sẽ trở thành bạn thân của nhau không?',
    'Kỉ niệm đẹp nhất của hai đứa là gì?',
    'Cậu đã bao giờ nghĩ mình sẽ không thể chơi với nhau tiếp không?',
    'Điều gì mình đã làm khiến cậu thấy vui vẻ nhất?',
    'Cậu muốn bọn mình làm gì/đi đâu cùng nhau nhất',
    'Sáu',
    'Bảy',
    'Tám',
    'Chín', 
    'Mười'
];

for(let i = 0; i < getSelectItems.length; i++) {
    getSelectItems[i].onclick = function(e){
        Object.assign(getSelectItemLove.style, {
            display: "none",
            animation: " blur 0.25s linear"
        });
        Object.assign(getSelectItemFriend.style, {
            display: "none",
            animation: " blur 0.25s linear"
        });
        Object.assign(getNextBtn.style, {
            display: "block",
            animation: "see 0.25s linear"
        });
        if(e.target.innerText === 'Tình yêu') {
            Object.assign(getCardImgLove.style, {
                display: "inline-block",
                animation: " see 0.4s linear"
            });
        } else {
            Object.assign(getCardImgFriend.style, {
                display: "inline-block",
                animation: " see 0.4s linear"
            });
        }
        getBackBtn.style.display = 'inline-block';
    }
}

getBackBtn.onclick = function() {
    Object.assign(getSelectItemLove.style, {
        display: "inline-block",
        animation: " see 0.25s linear"
    });
    Object.assign(getSelectItemFriend.style, {
        display: "inline-block",
        animation: " see 0.25s linear"
    });
    Object.assign(getCardImgLove.style, {
        display: "none",
        animation: " blur 0.4s linear"
    });
    Object.assign(getCardImgFriend.style, {
        display: "none",
        animation: " blur 0.4s linear"
    });
    Object.assign(getCardImgContent.style, {
        display: "none",
        animation: " blur 0.4s linear"
    });
    Object.assign(getNextBtn.style, {
        display: "none",
        animation: "blur 0.25s linear"
    });
    if(getSelectItemLove.style.display == 'inline-block') {
        getBackBtn.style.display = 'none';
    }
}

getCardImgLove.onclick = function() {
    let randomNumber = Math.floor(Math.random() * 10); // tao so ngau nhien tu 0 - 9
    getCardImgContent.innerHTML = loveQuestions[randomNumber];
    getCardImgLove.style.animation = "spin 1s linear, blur 0.5s linear 1s";
    setTimeout(function(){
        getCardImgLove.style.display = 'none';
        Object.assign(getCardImgContent.style,{
            display: 'inline-block',
            backgroundColor: '#e66767',
            animation: " see 0.25s linear"
        });
    },700)
    sessionStorage.setItem('status', 'love');
    sessionStorage.setItem('clicked', '1');
}

getCardImgFriend.onclick = function() {
    let randomNumber = Math.floor(Math.random() * 10); // tao so ngau nhien tu 0 - 9
    getCardImgContent.innerHTML = friendQuestions[randomNumber];
    getCardImgFriend.style.animation = "spin 1s linear, blur 0.5s linear 1s";
    setTimeout(function(){
        getCardImgFriend.style.display = 'none';
        Object.assign(getCardImgContent.style,{
            display: 'inline-block',
            backgroundColor: '#f5cd79',
            animation: " see 0.25s linear"
        });
    },700)
    sessionStorage.setItem('status', 'friend');
    sessionStorage.setItem('clicked', '1');
}

getNextBtn.onclick = function() {
    if(sessionStorage.getItem('clicked') == 1) {
        if(sessionStorage.getItem('status') === 'love') {
            getCardImgContent.style.animation = "spin 1s linear, blur 0.5s linear 1s";
            setTimeout(function(){
                getCardImgContent.style.display = 'none';
                Object.assign(getCardImgLove.style,{
                    display: 'inline-block',
                    animation: " see 0.25s linear"
                });
            },700)
        }else {
            getCardImgContent.style.animation = "spin 1s linear, blur 0.5s linear 1s";
            setTimeout(function(){
                getCardImgContent.style.display = 'none';
                Object.assign(getCardImgFriend.style,{
                    display: 'inline-block',
                    animation: " see 0.25s linear"
                });
            },700)
        }
    }
}